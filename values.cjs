// const testObject = require('./objects.cjs');

// function values(obj) {
//     // Return all of the values of the object's own properties.
//     // Ignore functions
//     // http://underscorejs.org/#values
// }

const valueArray = [];

function values(obj){
    for(let key in obj){
        valueArray.push(obj[key]);
    }
    return valueArray;
}
module.exports = values;