// function pairs(obj) {
//     // Convert an object into a list of [key, value] pairs.
//     // http://underscorejs.org/#pairs
// }

let pairsArray = [];
function pairs(obj){
    for(let key in obj){
        let array = [key,obj[key]];
        pairsArray.push(array);
    }
    return pairsArray;
}

module.exports = pairs;