// function mapObject(obj, cb) {
//     // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
//     // http://underscorejs.org/#mapObject
// }

function mapObject(obj,cb){
    const result = {};
    for(let key in obj){
        if(typeof obj[key] != 'number'){
            result[key] = obj[key];
        }else{
            result[key] = cb(obj[key]);
        }
        // if(value == "undefined" || value == NaN){
        //     result.key = "undefined";
        // }else{
        // }
    }
    return result;
    
}

module.exports = mapObject;

// const arr = [1,2,3,4,5];

// function double(val){
//     return val*2;
// }

// function map(array, cb){
//     const result = [];
//     for(let i = 0;i<array.length;i++){
//         result.push(double(array[i]));
//     }
//     return result;
// }

// const ans = map(arr,double);
// console.log(ans);